// EXTERNAL LIBS

const express = require('express');
const path = require('path');
const http = require('http');
const fs = require('fs');
const ejs = require('ejs');
const bodyParser = require('body-parser');



// LOCAL LIBS

const configConstants = require('./lib/Config.js');



// INIT

const server = express();
server.use(bodyParser.urlencoded({extended : true}));
server.use(bodyParser.json());
server.use(express.static(__dirname + '/public'));



// SERVER START

server.listen(configConstants.WEB_PORT);
console.log(configConstants.MSG_WEB_NAME + configConstants.MSG_SERVER_UP + configConstants.WEB_PORT);



// CALL THE STATIC HTML FOR CONFIGURATION PAGE (GET)

server.get('/', function(request, response)
{
	response.status(200).sendFile(path.join(__dirname + '/public/templates/Configuration.html'));
});



// BUILD PLAY PAGE (GET)

server.get('/play', function(request, response)
{	
	const gameId = request.query.game_id;
	var activePlayerSymbol;
	var cellsStatus;
	var winner = request.query.winner ? request.query.winner : undefined;
	var isGridFull;

	// get the active player
	var options =
	{
		host: configConstants.API_HOST,
		port: configConstants.API_PORT,		
		path: '/getActivePlayer?game_id=' + gameId,
	};
	http.request(options, function(response2)
	{
		response2.on('data', function (chunk)
		{
			try
			{
				activePlayerSymbol = JSON.parse(chunk)[0].active_player_id;
			}
			catch(error)
			{
				response.sendStatus(503);
				console.log(configConstants.MSG_ERROR_GENERIC, error);
				return;
			}
			// get cells status
			options =
			{
				host: configConstants.API_HOST,
				port: configConstants.API_PORT,
				
				path: '/getStatus?game_id=' + gameId,
			};
			http.request(options, function(response3)
			{
				response3.on('data', function (chunk)
				{
					try
					{
						cellsStatus = JSON.parse(chunk);
						buildAndSendPage();
					}
					catch(error)
					{
						response.sendStatus(503);
						console.log(configConstants.MSG_ERROR_GENERIC, error);
						return;
					}
				});
			}).end();
			// check if grid is full
			options =
			{
				host: configConstants.API_HOST,
				port: configConstants.API_PORT,
				path: '/isGridFull?game_id=' + gameId,
			};
			http.request(options, function(response3)
			{
				response3.on('data', function (chunk)
				{
					try
					{
						isGridFull = JSON.parse(chunk)[0].grid_full;
						buildAndSendPage();
					}
					catch(error)
					{
						response.sendStatus(503);
						console.log(configConstants.MSG_ERROR_GENERIC, error);
						return;
					}
				});
			}).end();

		});
	}).end();
	
	// build the page
	function buildAndSendPage()
	{
		if(gameId!=undefined && activePlayerSymbol!=undefined && cellsStatus != undefined && isGridFull != undefined)
		{			
			response.status(200).send(ejs.render(fs.readFileSync('public/templates/Play.ejs').toString(), {gameId, activePlayerSymbol, cellsStatus, winner, isGridFull}));
		}
	}

});



// SOLVE A NEW GAME CREATION (POST) RETURN THE PLAY PAGE UPDATED

server.post('/create_new_game', function(request, response)
{
	const options =
	{
		host: configConstants.API_HOST,
		port: configConstants.API_PORT,
		path: '/createNewGame?players_amount=' + request.body.players_amount + '&battlefield_size=' + request.body.battlefield_size,
	};

	http.request(options, function(response2)
	{
		response2.on('data', function (chunk)
		{
			try
			{
				const result = JSON.parse(chunk);
				const game_id = result[0].game_id;
				response.redirect('/play?game_id=' + game_id);
			}
			catch(error)
			{
				response.sendStatus(503);
				console.log(configConstants.MSG_ERROR_GENERIC, error);
				return;
			}
		});
	}).end();

});



// SOLVE A PLAYER MOVE (POST) RETURN THE PLAY PAGE UPDATED

server.post('/make_move', function(request, response)
{
	var options =
	{
		host: configConstants.API_HOST,
		port: configConstants.API_PORT,
		path: '/makeMove?game_id=' + request.query.game_id + '&x_position=' + request.query.x_position + '&y_position=' + request.query.y_position + '&symbol=' + request.query.symbol,
	};
	
	http.request(options, function(response2)
	{		
		// check if player has won
		options =
		{
			host: configConstants.API_HOST,
			port: configConstants.API_PORT,
			path: '/hasPlayerWon?game_id=' + request.query.game_id + '&symbol=' + request.query.symbol,
		};
		http.request(options, function(response3)
		{
			response3.on('data', function (chunk)
			{
				try
				{
					if(JSON.parse(chunk)[0].player_won)
					{
						response.redirect('/play?game_id=' + request.query.game_id + '&winner=' + request.query.symbol);
					}
					else
					{
						response.redirect('/play?game_id=' + request.query.game_id);
					}
				}	
				catch(error)
				{
					response.sendStatus(503);
					console.log(configConstants.MSG_ERROR_GENERIC, error);
					return;
				}
			});
		}).end();
	}).end();
});



// SOLVE A GAME RESET (POST) RETURN THE PLAY PAGE UPDATED

server.post('/reset', function(request, response)
{
	const options =
	{
		host: configConstants.API_HOST,
		port: configConstants.API_PORT,
		path: '/resetGame?game_id=' + request.query.game_id,
	};
	
	http.request(options, function(response2)
	{
		response.redirect('/play?game_id=' + request.query.game_id);
	}).end();
});