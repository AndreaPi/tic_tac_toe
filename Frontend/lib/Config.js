module.exports =
{
	// COMMONS
	
	MSG_SERVER_UP: ' running on port ',
	MSG_ERROR_GENERIC: 'Catch an error: ',
	
	
	
	// API
	
	API_PORT: 3000,
	API_HOST: 'localhost',
	
		
	
	// WEB
	
	WEB_PORT: 80,
	
	MSG_WEB_NAME: 'WEB Manager',
	
}
