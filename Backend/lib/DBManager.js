// EXPORTS

module.exports.createNewGame = createNewGame;
module.exports.resetGame = resetGame;
module.exports.getCellsFromGameId = getCellsFromGameId;
module.exports.fillCell = fillCell;
module.exports.getActivePlayer = getActivePlayer;
module.exports.hasPlayerWon = hasPlayerWon;
module.exports.isGridFull = isGridFull;



// EXTERNAL LIBS

const util = require( 'util' );
const mysql = require('mysql');



// LOCAL LIBS

const configConstants = require('./Config.js');



// DATABASE CONNECTION

function createDB()
{
	try
	{
		const connection = mysql.createConnection(configConstants.DB_CONFIG);
		return {
			query( sql, args )
			{
			  return util.promisify(connection.query).call(connection, sql, args);
			},
			close()
			{
			  return util.promisify(connection.end).call(connection);
			}
		};  
	}
	catch(error)
	{
		console.log(configConstants.MSG_DB_ERROR, error);
	}
}



// INSERT A NEW GAME IN DB / RETURN ID OF THE GAME

async function createNewGame(playersAmount, battlefieldSize)
{
	const db = createDB();
	try
	{
		var gameId;
		await db.query(
			'INSERT INTO games (players_number) VALUES(?);',
			[playersAmount]
			);
		var result = await db.query(
			'SELECT game_id FROM games ORDER BY game_id DESC LIMIT 1;'
			);
		gameId = result[0].game_id;
		for (let xPosition = 0; xPosition < battlefieldSize; xPosition++)
		{
			for (let yPosition = 0; yPosition < battlefieldSize; yPosition++)
			{
				await db.query(
					'INSERT INTO cells (game_id, x_position, y_position, symbol) VALUES (?,?,?,?);',
					[gameId, xPosition, yPosition, configConstants.SYMBOL_EMPTY]
				);
			}
		}
		await db.close();
		return result;		
	}
	catch(error)
	{
		await db.close();
		console.log(configConstants.MSG_DB_ERROR, error);
	}
};



// RESET A GAME BY ID / NO RETURN

async function resetGame(gameId)
{
	const db = createDB();
	try
	{
		await db.query(
			'UPDATE cells SET symbol = ? WHERE game_id = ?;',
			[configConstants.SYMBOL_EMPTY, gameId]
		);
		result = await db.query(
			'UPDATE games SET active_player_id = 1 WHERE game_id = ?;',
			[gameId]
		);
		await db.close();
	}
	catch(error)
	{
		await db.close();
		console.log(configConstants.MSG_DB_ERROR, error);
	}
};



// GET ALL CELLS OF A GAME / RETURN CELLS OF THE GAME

async function getCellsFromGameId(gameId)
{
	const db = createDB();
	try
	{
		var result;
		result = await db.query(
			'SELECT x_position, y_position, symbol FROM cells WHERE game_id = ?;',
			[gameId]
			);
		await db.close();
		return result;
	}
	catch(error)
	{
		await db.close();
		console.log(configConstants.MSG_DB_ERROR, error);
	}
};



// FILL A CELL / NO RETURN

async function fillCell(gameId, xPosition, yPosition, symbol)
{
	const db = createDB();
	try
	{
		// fill cell
		var result = await db.query(
			'UPDATE cells SET symbol = ? WHERE game_id = ? && x_position = ? && y_position = ? && symbol = ?;',
			[symbol, gameId, xPosition, yPosition, configConstants.SYMBOL_EMPTY]
			);
		
		// if success, change player turn
		if(result.affectedRows==1)
		{
			result = await db.query(
				'SELECT players_number, active_player_id FROM games WHERE game_id = ?;',
				[gameId]
			);
				
			var playerTurn;
			if(result[0].active_player_id == result[0].players_number)
			{
				playerTurn = 1;
			}
			else
			{
				playerTurn = result[0].active_player_id + 1;
			}
			
			result = await db.query(
				'UPDATE games SET active_player_id = ? WHERE game_id = ?;',
				[playerTurn, gameId]
			);
		}
		await db.close();				
	}
	catch(error)
	{
		await db.close();
		console.log(configConstants.MSG_DB_ERROR, error);
	}
};



// GET ACTIVE PLAYER / RETURN ACTIVE PLAYER ID

async function getActivePlayer(gameId)
{
	const db = createDB();
	try
	{
		var result;
		result = await db.query(
			'SELECT active_player_id FROM games WHERE game_id = ?;',
			[gameId]
			);
		await db.close();
		return result;
	}
	catch(error)
	{
		await db.close();
		console.log(configConstants.MSG_DB_ERROR, error);
	}
};



// CHECK IF TARGET PLAYER HAS WON / RETURN BOOLEAN

async function hasPlayerWon(gameId, playerSymbol)
{
	const db = createDB();
	try
	{
		var result;
		result = await db.query(
			'CALL HasPlayerWon(?, ?);',
			[gameId, playerSymbol]
			);		
		const jsonString = JSON.parse('[{"player_won": ' + result[0][0].player_won + '}]');
		return jsonString;
	}
	catch(error)
	{
		await db.close();
		console.log(configConstants.MSG_DB_ERROR, error);
	}
};



// CHECK IF FIELD IS FULL / RETURN BOOLEAN

async function isGridFull(gameId)
{
	const db = createDB();
	try
	{
		var result;
		result = await db.query(
			'SELECT COUNT(*) AS found_cells FROM cells WHERE game_id = ? && symbol = ?;',
			[gameId, configConstants.SYMBOL_EMPTY]
			);
		if(result[0].found_cells == 0)
		{
			await db.close();
			return JSON.parse('[{"grid_full": true}]');;
		}
		else
		{
			await db.close();
			return JSON.parse('[{"grid_full": false}]');;
		}
	}
	catch(error)
	{
		await db.close();
		console.log(configConstants.MSG_DB_ERROR, error);
	}
};
