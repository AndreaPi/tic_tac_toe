module.exports =
{
	// COMMONS
	
	MSG_SERVER_UP: ' running on port ',
	MSG_ERROR_GENERIC: 'Catch an error: ',
	
	
	
	// API
	
	API_PORT: 3000,
	
	MIN_PLAYERS: 2,
	MAX_PLAYERS: 4,
	MIN_BATTLEFIELD_SIZE: 2,
	MAX_BATTLEFIELD_SIZE: 10,
	
	MSG_API_NAME: 'API Manager',

	
	
	// DATABASE
	
	SYMBOL_EMPTY: ' ',
	
	MSG_DB_ERROR: 'DB connection error',
	
	DB_CONFIG:
	{
		host     : 'localhost', 	// Modify DB server address
		port     : '3306',			// Modify DB server port
		user     : 'root',			// Modify DB server username
		password : 'root',			// Modify DB server password
		database : 'tic_tac_toe',
		multipleStatements: true
	},
}
