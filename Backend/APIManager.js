// EXTERNAL LIBS

const express = require('express');



// LOCAL LIBS

const configConstants = require('./lib/Config.js');
const DBManager = require('./lib/DBManager.js');



// INIT

var server = express();



// SERVER START

server.listen(configConstants.API_PORT);
console.log(configConstants.MSG_API_NAME + configConstants.MSG_SERVER_UP + configConstants.API_PORT);



// CREATE NEW GAME (GET) RETURN ID OF THE GAME CREATED

server.get('/createNewGame', function(request, response)
{
	// if valid input
	var playersAmount = parseInt(request.query.players_amount);
	var battlefieldSize = parseInt(request.query.battlefield_size);
	
	if(	configConstants.MIN_PLAYERS <= playersAmount && playersAmount <= configConstants.MAX_PLAYERS &&
		configConstants.MIN_BATTLEFIELD_SIZE <= battlefieldSize && battlefieldSize <= configConstants.MAX_BATTLEFIELD_SIZE)		
	{
		// call DB
		(async function() {
			try
			{
				const gameId = await DBManager.createNewGame(playersAmount, battlefieldSize);
				response.status(200).json(gameId);
			}
			catch(error)
			{
				response.sendStatus(400);
				console.log(configConstants.MSG_ERROR_GENERIC, error);
			}
		})();
	}
	else
	{
		response.sendStatus(400);
	}
});



// RESET A GAME (GET) RETURN ONLY STATUS

server.get('/resetGame', function(request, response)
{
	// if valid input
	var gameId = parseInt(request.query.game_id);
	
	if (!isNaN(gameId) && parseInt(gameId) > 0)		
	{
		// call DB
		(async function() {
			try
			{
				await DBManager.resetGame(gameId);
				response.sendStatus(200);
			}
			catch(error)
			{
				response.sendStatus(400);
				console.log(configConstants.MSG_ERROR_GENERIC, error);
			}
		})();
	}
	else
	{
		response.sendStatus(400);
	}
});



// GET STATUS OF CELLS (GET) RETURN CELLS OF THE GAME IN A JSON

server.get('/getStatus', function(request, response)
{
	// if valid input
	var gameId = parseInt(request.query.game_id);
	
	if (!isNaN(gameId) && parseInt(gameId) > 0)
	{
		// call DB
		(async function() {
			try
			{
				const cellsStatus = await DBManager.getCellsFromGameId(gameId);
				response.status(200).json(cellsStatus);
			}
			catch(error)
			{
				response.sendStatus(400);
				console.log(configConstants.MSG_ERROR_GENERIC, error);
			}
		})();
	}
	else
	{
		response.sendStatus(400);
	}
});



// CHANGE CELL SYMBOL (GET) RETURN ONLY STATUS

server.get('/makeMove', function(request, response)
{
	// if valid input
	var gameId = request.query.game_id;
	var xPosition = request.query.x_position;
	var yPosition = request.query.y_position;
	var symbol = request.query.symbol;
	
	if (!isNaN(gameId) && parseInt(gameId) > 0 &&
		!isNaN(xPosition) && parseInt(xPosition) >= 0 &&
		!isNaN(yPosition) && parseInt(yPosition) >= 0 &&
		symbol.length === 1)		
	{
		// call DB
		(async function() {
			try
			{
				await DBManager.fillCell(gameId, xPosition, yPosition, symbol);
				response.sendStatus(200);
			}
			catch(error)
			{
				response.sendStatus(400);
				console.log(configConstants.MSG_ERROR_GENERIC, error);
			}
		})();
	}
	else
	{
		response.sendStatus(400);
	}
});



// GET ACTIVE PLAYER (GET) RETURN ID OF PLAYER IN A JSON

server.get('/getActivePlayer', function(request, response)
{
	// if valid input
	var gameId = parseInt(request.query.game_id);
	
	if (!isNaN(gameId) && parseInt(gameId))
	{
		// call DB
		(async function() {
			try
			{
				const activePlayer = await DBManager.getActivePlayer(gameId);
				response.status(200).json(activePlayer);
			}
			catch(error)
			{
				response.sendStatus(400);
				console.log(configConstants.MSG_ERROR_GENERIC, error);
			}
		})();
	}
	else
	{
		response.sendStatus(400);
	}
});



// CHECK IF TARGET PLAYER WON (GET) RETURN BOOLEAN IN A JSON

server.get('/hasPlayerWon', function(request, response)
{
	// if valid input
	var gameId = parseInt(request.query.game_id);
	var symbol = request.query.symbol;
	
	if (!isNaN(gameId) && parseInt(gameId) > 0 && symbol.length === 1)		
	{
		// call DB
		(async function() {
			try
			{
				const hasPlayerWon = await DBManager.hasPlayerWon(gameId, symbol);
				response.status(200).json(hasPlayerWon);
			}
			catch(error)
			{
				response.sendStatus(400);
				console.log(configConstants.MSG_ERROR_GENERIC, error);
			}
		})();
	}
	else
	{
		response.sendStatus(400);
	}
});



// CHECK IF FIELD IS FULL (GET) RETURN BOOLEAN IN A JSON

server.get('/isGridFull', function(request, response)
{
	// if valid input
	var gameId = parseInt(request.query.game_id);
	
	if (!isNaN(gameId) && parseInt(gameId))
	{
		// call DB
		(async function() {
			try
			{
				const gridFull = await DBManager.isGridFull(gameId);
				response.status(200).json(gridFull);
			}
			catch(error)
			{
				response.sendStatus(400);
				console.log(configConstants.MSG_ERROR_GENERIC, error);
			}
		})();
	}
	else
	{
		response.sendStatus(400);
	}
});
