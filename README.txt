Set the environment:
- install node and required packages
- install MySQL server and create the database (copy/paste "DATABASE CREATION" section inside console)
- config the project modifying file "/lib/Config.js"

Start servers (need 2 instances of console to run both servers):
- for API server: node APIManager.js
- for WEB server: node WEBManager.js

Run:
- open a browser and point to server:port (default -> localhost:80)



// -------------------- REQUIRED NODE PACKAGES -------------------- //



npm install express
npm install mysql
npm install util
npm install body-parser
npm install path
npm install fs
npm install ejs
npm install http



// -------------------- DATABASE CREATION -------------------- //



DROP DATABASE tic_tac_toe;

CREATE DATABASE tic_tac_toe;

USE tic_tac_toe;

CREATE TABLE games(
	game_id INT NOT NULL AUTO_INCREMENT,
	players_number INT NOT NULL DEFAULT 2,
	active_player_id INT NOT NULL DEFAULT 1,
	PRIMARY KEY(game_id)
)ENGINE=InnoDb;

CREATE TABLE cells(
	cell_id INT NOT NULL AUTO_INCREMENT,
	game_id INT NOT NULL,
	x_position INT NOT NULL,
	y_position INT NOT NULL,
	symbol CHAR(1) NOT NULL,
	PRIMARY KEY(cell_id),
	FOREIGN KEY(game_id) REFERENCES games(game_id) ON DELETE CASCADE
)ENGINE=InnoDb;


 
DELIMITER $$

CREATE PROCEDURE HasPlayerWon(IN gameId INT, playerSymbol VARCHAR(255))
this:BEGIN
    DECLARE grid_dimension INT;    
	DECLARE found_cells INT;    
	DECLARE x INT; 
	DECLARE y INT; 
	
	SELECT COUNT(*) FROM cells WHERE game_id = gameId && x_position = 0 INTO grid_dimension;
	
	SET x = 0;
	WHILE x < grid_dimension DO 
		SELECT COUNT(*) FROM cells WHERE game_id = gameId && x_position = x && symbol = playerSymbol INTO found_cells;
		IF found_cells = grid_dimension THEN
			SELECT 'true' AS player_won;
			LEAVE this;
		END IF;
		SET x = x + 1;
	END WHILE;
	
	SET y = 0;
	WHILE y < grid_dimension DO 
		SELECT COUNT(*) FROM cells WHERE game_id = gameId && y_position = y && symbol = playerSymbol INTO found_cells;
		IF found_cells = grid_dimension THEN
			SELECT 'true' AS player_won;
			LEAVE this;
		END IF;
		SET y = y + 1;
	END WHILE;
	
	SELECT COUNT(*) FROM cells WHERE game_id = gameId && x_position = y_position && symbol = playerSymbol INTO found_cells;
	IF found_cells = grid_dimension THEN
		SELECT 'true' AS player_won;
		LEAVE this;
	END IF;
	
	SELECT COUNT(*) FROM cells WHERE game_id = gameId && x_position = grid_dimension-y_position && symbol = playerSymbol INTO found_cells;
	IF found_cells = grid_dimension THEN
		SELECT 'true' AS player_won;
		LEAVE this;
	END IF;
	
	SELECT 'false' AS player_won;
	
END$$

DELIMITER ;
